package org.sunyata.peacefulplaces;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.sunyata.peacefulplaces.database.DbSchema;
import org.sunyata.peacefulplaces.database.PeacefulPlaceCursorWrapper;
import org.sunyata.peacefulplaces.database.PeacefulPlaceDbHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by sunyata on 2016-05-21.
 */
public class PeacefulPlaceCollection {

    private static PeacefulPlaceCollection sCollection;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    //Singleton
    public static PeacefulPlaceCollection get(Context iContext){

        if(sCollection == null){
            sCollection = new PeacefulPlaceCollection(iContext);
        }

        return sCollection;
    }

    private PeacefulPlaceCollection(Context iContext){

        mContext = iContext;
        mDatabase = new PeacefulPlaceDbHelper(iContext).getWritableDatabase();


    }

    public ContentValues getContentValues(PeacefulPlace iPlace){

        ContentValues retContentValues = new ContentValues();

        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.UUID, iPlace.getUuid().toString());
        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.LATITUDE, iPlace.getLatitude());
        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.LONGITUDE, iPlace.getLongitude());
        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.TITLE, iPlace.getTitle());
        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.DESCRIPTION, iPlace.getDescription());
        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.PRIVACY, iPlace.getPrivacy());
        retContentValues.put(DbSchema.PeacefulPlaceTable.Cols.QUIET, iPlace.getQuiet());

        return retContentValues;

    }


    public void addOrUpdatePlace(PeacefulPlace iPlace){
        ContentValues tContentValues = getContentValues(iPlace);
        if(this.getPlace(iPlace.getUuid()) != null){
            mDatabase.update(DbSchema.PeacefulPlaceTable.NAME, tContentValues,
                    DbSchema.PeacefulPlaceTable.Cols.UUID + " = ?",
                    new String[]{iPlace.getUuid().toString()});
        }else{
            mDatabase.insert(DbSchema.PeacefulPlaceTable.NAME, null, tContentValues);

        }
    }

    public void deletePlace(UUID iUuid){

        mDatabase.delete(DbSchema.PeacefulPlaceTable.NAME,
                DbSchema.PeacefulPlaceTable.Cols.UUID + " = ?",
                new String[]{iUuid.toString()});
    }

    /*
    public void addPlace(PeacefulPlace iPlace){

        ContentValues tContentValues = getContentValues(iPlace);

        mDatabase.insert(DbSchema.PeacefulPlaceTable.NAME, null, tContentValues);

    }

    public void updatePlace(PeacefulPlace iPlace){
        ContentValues tContentValues = getContentValues(iPlace);
        mDatabase.update(DbSchema.PeacefulPlaceTable.NAME, tContentValues,
                DbSchema.PeacefulPlaceTable.Cols.GOOGLE_MAPS_MARKER_ID + " = ?",
                new String[]{iPlace.getMapMarkerId()});
    }
    */

    public PeacefulPlace getPlace(UUID iUuid){

        PeacefulPlaceCursorWrapper tCursorWrapper = queryPeacefulPlaces(
                DbSchema.PeacefulPlaceTable.Cols.UUID + " = '" + iUuid.toString() + "'",
                null
        );
        /*
        There's a problem when using the String array like this "new String[]{iUuid.toString()}"
        (Unknown why this problem appears)
         */

        try{
            if(tCursorWrapper.getCount() == 0){
                return null;
            }

            tCursorWrapper.moveToFirst();

            return tCursorWrapper.getPeacefulPlace();

        }finally {
            tCursorWrapper.close();
        }

    }

    public List<PeacefulPlace> getPlaces(){
        List<PeacefulPlace> retPlacesList = new ArrayList<>();

        PeacefulPlaceCursorWrapper tCursorWrapper = queryPeacefulPlaces(null, null);
        tCursorWrapper.moveToFirst();
        while(!tCursorWrapper.isAfterLast()){
            retPlacesList.add(tCursorWrapper.getPeacefulPlace());
            tCursorWrapper.moveToNext();
        }
        tCursorWrapper.close();

        return retPlacesList;
    }

    private PeacefulPlaceCursorWrapper queryPeacefulPlaces(String iWhereClauseSg, String[] iWhereArgsAr){
        Cursor tCursor = mDatabase.query(
                DbSchema.PeacefulPlaceTable.NAME,
                null,
                iWhereClauseSg,
                iWhereArgsAr,
                null, null, null);

        return new PeacefulPlaceCursorWrapper(tCursor);
    }

}
