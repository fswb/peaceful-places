package org.sunyata.peacefulplaces.database;

/**
 * Created by sunyata on 2016-05-21.
 */
public class DbSchema {

    public static final class PeacefulPlaceTable {

        public static final String NAME = "locations";

        public static final class Cols {
            public static final String UUID = "uuid";
            ///public static final String GOOGLE_MAPS_MARKER_ID = "google_maps_marker_id";
            public static final String LATITUDE = "latitude";
            public static final String LONGITUDE = "longitude";
            public static final String TITLE = "title";
            public static final String DESCRIPTION = "description";
            public static final String PRIVACY = "privacy";
            public static final String QUIET = "quiet";
        }


    }
}
