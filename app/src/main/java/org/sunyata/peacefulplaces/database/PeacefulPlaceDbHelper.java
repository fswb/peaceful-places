package org.sunyata.peacefulplaces.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.sunyata.peacefulplaces.PeacefulPlace;
import org.sunyata.peacefulplaces.database.DbSchema;

/**
 * Created by sunyata on 2016-05-21.
 */
public class PeacefulPlaceDbHelper extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 8;
    private static final String DATABASE_NAME = "location_collection.db";

    public PeacefulPlaceDbHelper(Context iContext){

        super(iContext, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    //TODO: Move this method into the class with the table
    public void createTable(SQLiteDatabase db){
        db.execSQL("create table " + DbSchema.PeacefulPlaceTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                DbSchema.PeacefulPlaceTable.Cols.UUID + " text not null default ''" + ", " +
                DbSchema.PeacefulPlaceTable.Cols.LATITUDE + " real not null default " + PeacefulPlace.POS_NOT_SET + ", " +
                DbSchema.PeacefulPlaceTable.Cols.LONGITUDE + " real not null default " + PeacefulPlace.POS_NOT_SET + ", " +
                DbSchema.PeacefulPlaceTable.Cols.TITLE + " text not null default ''" + ", " +
                DbSchema.PeacefulPlaceTable.Cols.DESCRIPTION + " text not null default ''" + ", " +
                DbSchema.PeacefulPlaceTable.Cols.PRIVACY + " integer not null default 0" + ", " +
                DbSchema.PeacefulPlaceTable.Cols.QUIET + " integer not null default 0" +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists " + DbSchema.PeacefulPlaceTable.NAME);
        createTable(db);


    }
}
