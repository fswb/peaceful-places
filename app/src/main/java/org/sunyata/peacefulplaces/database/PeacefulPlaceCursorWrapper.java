package org.sunyata.peacefulplaces.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import org.sunyata.peacefulplaces.PeacefulPlace;
import org.sunyata.peacefulplaces.database.DbSchema;

import java.util.UUID;

/**
 * Created by sunyata on 2016-05-23.
 */
public class PeacefulPlaceCursorWrapper extends CursorWrapper {

    public PeacefulPlaceCursorWrapper(Cursor iCursor) {
        super(iCursor);
    }

    /**
     * Gets the peaceful place from where the cursor is
     */
    public PeacefulPlace getPeacefulPlace(){
        ///String tMapMarkerIdSg = getString(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.GOOGLE_MAPS_MARKER_ID));
        String tUuidStringSg = getString(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.UUID));
        double tLatitudeDe = getDouble(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.LATITUDE));
        double tLongitudeDe = getDouble(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.LONGITUDE));
        String tTitleSg = getString(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.TITLE));
        String tDescriptionSg = getString(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.DESCRIPTION));
        int tPrivacyIt = getInt(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.PRIVACY));
        int tQuietIt = getInt(getColumnIndex(DbSchema.PeacefulPlaceTable.Cols.QUIET));

        PeacefulPlace tPeacefulPlace = new PeacefulPlace(UUID.fromString(tUuidStringSg));
        tPeacefulPlace.setLatitude(tLatitudeDe);
        tPeacefulPlace.setLongitude(tLongitudeDe);
        tPeacefulPlace.setTitle(tTitleSg);
        tPeacefulPlace.setDescription(tDescriptionSg);
        tPeacefulPlace.setPrivacy(tPrivacyIt);
        tPeacefulPlace.setQuiet(tQuietIt);

        return tPeacefulPlace;
    }

}
