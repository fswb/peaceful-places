package org.sunyata.peacefulplaces;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    ////FragmentActivity

    private GoogleMap mMap;

    private HashMap<String, UUID> mMarkerIdToDbUuidHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);




    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        ////////LatLng tGothenburgLatLng = new LatLng(57 + (1/60) * 42, 11 + (1/60) * 58);
        LatLng tGothenburgLatLng = new LatLng(57.7089, 11.9746);
        mMap.addMarker(new MarkerOptions().position(tGothenburgLatLng).title("Little London (aka Göteborg)"));

        LatLng tDharamsalaLatLng = new LatLng(32.2190, 76.3234);
        mMap.addMarker(new MarkerOptions().position(tDharamsalaLatLng).title("Dharamsala (where the Dalai Lama lives)"));



        ////mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tGothenburgLatLng, 11));


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //////return;
        }else{
            mMap.setMyLocationEnabled(true);
        }


        //Enable the "show current location" button that will be displayed in the upper left corner
        mMap.setOnMapClickListener(this);


        //Please note that onMyLocationChanged has been deprecated

        LocationManager tLocationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        Criteria tCriteria = new Criteria();
        String tProviderSg = tLocationManager.getBestProvider(tCriteria, false);
        Location tLocation = null;
        if(tProviderSg != null){
            tLocation = tLocationManager.getLastKnownLocation(tProviderSg);
        }else{
            String tWarningText = "No location provider, please activate GPS or another location provider option";
            Toast.makeText(MapsActivity.this, tWarningText, Toast.LENGTH_LONG).show();
            Log.w("asdf", tWarningText);
        }
        LatLng tUserLocationLatLng;
        if(tLocation != null){
            tUserLocationLatLng = new LatLng(tLocation.getLatitude(), tLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tUserLocationLatLng, 14));
        }else{
            String tErrorText = "ERROR: Attempt to invoke virtual method 'double android.location.Location.getLatitude()' on a null object reference";
            Toast.makeText(MapsActivity.this, tErrorText, Toast.LENGTH_LONG).show();
            Log.e("asdf", tErrorText);
        }






        mMap.setInfoWindowAdapter(
                new MyInfoWindowAdapter(
                        getLayoutInflater()));




        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker iMarker) {
                Intent i = new Intent(MapsActivity.this, PeacefulPlaceActivity.class);
                UUID tUuid = mMarkerIdToDbUuidHashMap.get(iMarker.getId());
                i.putExtra(PeacefulPlaceActivity.UUID_AS_STRING_EXTRA, tUuid.toString());
                startActivity(i);
            }
        });



        updateMarkersOnMap();


    }

    @Override
    public void onResume(){
        super.onResume();

        //Please note that we can't update the UI here since we don't know that the map is shown yet

    }

    private void updateMarkersOnMap(){

        //First we remove all markers, this is important in case one has just been removed
        mMap.clear();

        mMarkerIdToDbUuidHashMap = new HashMap<String, UUID>();

        MarkerOptions tMarkerOptions;

        List<PeacefulPlace> tPlacesList = PeacefulPlaceCollection.get(this).getPlaces();
        for(PeacefulPlace p : tPlacesList){

            //Adding the marker to the map

            tMarkerOptions = new MarkerOptions();
            tMarkerOptions.position(new LatLng(p.getLatitude(), p.getLongitude()));
            tMarkerOptions.draggable(false);
            tMarkerOptions.title(p.getTitle());
            tMarkerOptions.snippet(p.getDescription());

            ///mMap.addMarker(tMarkerOptions);

            Marker tAddedMarker = mMap.addMarker(tMarkerOptions);

            mMarkerIdToDbUuidHashMap.put(tAddedMarker.getId(), p.getUuid());
        }

    }

    @Override
    public void onMapClick(LatLng iLatLng) {

        Log.i("asdf", "latLng = " + iLatLng.toString());


        MarkerOptions tMarkerOptions = new MarkerOptions();
        tMarkerOptions.position(iLatLng); //remove and update all instead when getting back from the other activity?
        tMarkerOptions.draggable(false);
        ///tMarkerOptions.title("title test 1");
        ///tMarkerOptions.snippet("snippet test 1");

        Marker tMarker = mMap.addMarker(tMarkerOptions);


        //Starting create new location activity where details can be entered
        Intent i = new Intent(this, PeacefulPlaceActivity.class);
        ////////i.putExtra(PeacefulPlaceActivity.UUID_AS_STRING_EXTRA, );
        i.putExtra(PeacefulPlaceActivity.LATITUDE_DOUBLE_EXTRA, tMarker.getPosition().latitude);
        i.putExtra(PeacefulPlaceActivity.LONGITUDE_DOUBLE_EXTRA, tMarker.getPosition().longitude);
        startActivity(i);

        //Map will be updated when we get back because of the call to the update method at the end of the onMapReady method
    }


    private class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        LayoutInflater mLayoutInflater;

        public MyInfoWindowAdapter(LayoutInflater iLayoutInflater){
            mLayoutInflater = iLayoutInflater;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        //Customizing this rather than the getIntoWindow method above will keep the frame and background of the window
        @Override
        public View getInfoContents(Marker iMarker) {
            View retView = mLayoutInflater.inflate(R.layout.info_window, null);

            final Marker tMarker = iMarker;

            TextView tTitleTv = (TextView)retView.findViewById(R.id.info_window_title);
            TextView tDescriptionTv = (TextView)retView.findViewById(R.id.info_window_description);

            UUID tUuid = mMarkerIdToDbUuidHashMap.get(iMarker.getId());

            PeacefulPlace p = PeacefulPlaceCollection.get(MapsActivity.this).getPlace(tUuid);




            tTitleTv.setText(p.getTitle());
            tDescriptionTv.setText(p.getDescription());




            return retView;
        }
    }

/**
 * Using the example here
 * https://developer.android.com/guide/topics/ui/menus.html
 * rather than what is shown in the Big Nerd Ranch book
 */
    @Override
    public boolean onCreateOptionsMenu(Menu iMenu){
        getMenuInflater().inflate(R.menu.main_menu, iMenu);
        return true;
    }

}
