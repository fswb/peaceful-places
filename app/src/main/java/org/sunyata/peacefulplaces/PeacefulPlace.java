package org.sunyata.peacefulplaces;

import java.util.UUID;

/**
 * Created by sunyata on 2016-05-21.
 */
public class PeacefulPlace {

    public static final double POS_NOT_SET = 1000;

    private UUID mUuid;
    ///private String mMapMarkerIdSg;
    private double mLatitudeIt = 0;
    private double mLongitudeIt = 0;
    private String mTitleSg = "";
    private String mDescriptionSg = "";
    private int mPrivacyIt = 0;
    private int mQuietIt = 0;

    /*
    public PeacefulPlace(String iMapMarkerIdSg, double iLatitude, double iLongitude){
        mMapMarkerIdSg = iMapMarkerIdSg;
        mLatitudeIt = iLatitude;
        mLongitudeIt = iLongitude;
    }
    */

    public PeacefulPlace(UUID iUuid){
        mUuid = iUuid;
    }

/*
    public PeacefulPlace(String iMapMarkerIdSg){
        mMapMarkerIdSg = iMapMarkerIdSg;

    }
*/

    /*
    public String getMapMarkerId(){
        return mMapMarkerIdSg;
    }
    */

    public UUID getUuid(){
        return mUuid;
    }

    public double getLatitude(){
        return mLatitudeIt;
    }

    public void setLatitude(double iLatitudeIt){
        mLatitudeIt = iLatitudeIt;
    }

    public double getLongitude(){
        return mLongitudeIt;
    }

    public void setLongitude(double iLongitudeIt){
        mLongitudeIt = iLongitudeIt;
    }

    public String getTitle(){
        return mTitleSg;
    }

    public void setTitle(String iTitleSg){
        mTitleSg = iTitleSg;
    }

    public String getDescription(){
        return mDescriptionSg;
    }

    public void setDescription(String iDescriptionSg){
        mDescriptionSg = iDescriptionSg;
    }

    public int getPrivacy(){
        return mPrivacyIt;
    }

    public void setPrivacy(int iPrivacyIt){
        mPrivacyIt = iPrivacyIt;
    }

    public int getQuiet(){
        return mQuietIt;
    }

    public void setQuiet(int iQuietIt){
        mQuietIt = iQuietIt;
    }


}
