package org.sunyata.peacefulplaces;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.SeekBar;

import java.util.UUID;

public class PeacefulPlaceActivity extends AppCompatActivity {

    ///public static final String CREATION_EXTRA = "creation_extra";
    ///public static final String MAP_MARKER_ID_STRING_EXTRA = "map_marker_id_string_extra";
    public static final String UUID_AS_STRING_EXTRA = "uuid_as_string_extra";
    public static final String LATITUDE_DOUBLE_EXTRA = "latitude_double_extra";
    public static final String LONGITUDE_DOUBLE_EXTRA = "longitude_double_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);


        //updateUI();

    }

    @Override
    protected void onResume(){
        super.onResume();

        updateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu iMenu){
        getMenuInflater().inflate(R.menu.location_menu, iMenu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem iMenuItem){

        switch (iMenuItem.getItemId()){

            case R.id.pp_menu_save:
                saveToDatabase();
                finish();
                return true;

            case R.id.pp_menu_delete:

                PeacefulPlaceCollection.get(this).deletePlace(
                        UUID.fromString(getIntent().getStringExtra(UUID_AS_STRING_EXTRA)));

                //TODO: Handle the case when the place has not yet been added to the db

                //The map should be automatically updated

                finish();

                return true;
            default:
                return super.onOptionsItemSelected(iMenuItem);
        }

    }

    private void updateUI(){

        final EditText tTitleEt = (EditText)findViewById(R.id.location_title);
        final EditText tDescriptionEt = (EditText)findViewById(R.id.location_description);
        final EditText tLatitudeEt = (EditText)findViewById(R.id.location_latitude);
        final EditText tLongitudeEt = (EditText)findViewById(R.id.location_longitude);
        //final RatingBar tQuietRatingBar = (RatingBar)findViewById(R.id.location_ratingBar_quiet);

        String tUuidExtraString = getIntent().getStringExtra(UUID_AS_STRING_EXTRA);

        if(tUuidExtraString != null){
            PeacefulPlace tPeacefulPlace = PeacefulPlaceCollection.get(this).getPlace(
                    UUID.fromString(tUuidExtraString));
            tTitleEt.setText(tPeacefulPlace.getTitle());
            tDescriptionEt.setText(tPeacefulPlace.getDescription());
            tLatitudeEt.setText(Double.toString(tPeacefulPlace.getLatitude()));
            tLongitudeEt.setText(Double.toString(tPeacefulPlace.getLongitude()));

        }else{
            //No existing peaceful place (a new one will be created at save)

            double tLatitudeDe = getIntent().getDoubleExtra(LATITUDE_DOUBLE_EXTRA, PeacefulPlace.POS_NOT_SET);
            double tLongitudeDe = getIntent().getDoubleExtra(LONGITUDE_DOUBLE_EXTRA, PeacefulPlace.POS_NOT_SET);

            tLatitudeEt.setText(Double.toString(tLatitudeDe));
            tLongitudeEt.setText(Double.toString(tLongitudeDe));
        }


        //final SeekBar tPrivacySeekBar = (SeekBar)findViewById(R.id.location_seekBar_privacy);
        //final SeekBar tQuietSeekBar = (SeekBar)findViewById(R.id.location_seekBar_quiet);



    }

    /**
     * Two cases: We are either creating or updating the place in the db
     */
    private void saveToDatabase(){

        final EditText tTitleEt = (EditText)findViewById(R.id.location_title);
        final EditText tDescriptionEt = (EditText)findViewById(R.id.location_description);
        final EditText tLatitudeEt = (EditText)findViewById(R.id.location_latitude);
        final EditText tLongitudeEt = (EditText)findViewById(R.id.location_longitude);
        //final RatingBar tQuietRatingBar = (RatingBar)findViewById(R.id.location_ratingBar_quiet);

        /*
                PeacefulPlace tPeacefulPlace = PeacefulPlaceCollection.get(this).getPlace(
                getIntent().getStringExtra(MAP_MARKER_ID_STRING_EXTRA));

         */

        final PeacefulPlace tPeacefulPlace;
        String tUuidExtraString = getIntent().getStringExtra(UUID_AS_STRING_EXTRA);


        if(tUuidExtraString != null){
            tPeacefulPlace = new PeacefulPlace(UUID.fromString(tUuidExtraString));
        }else{
            tPeacefulPlace = new PeacefulPlace(UUID.randomUUID());
        }



        tPeacefulPlace.setLatitude(Double.parseDouble(tLatitudeEt.getText().toString()));
        tPeacefulPlace.setLongitude(Double.parseDouble(tLongitudeEt.getText().toString()));
        tPeacefulPlace.setTitle(tTitleEt.getText().toString());
        tPeacefulPlace.setDescription(tDescriptionEt.getText().toString());


        PeacefulPlaceCollection.get(PeacefulPlaceActivity.this).addOrUpdatePlace(
                tPeacefulPlace);


    }

}
