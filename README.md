Project status: Pre-alpha

# peaceful-places

Peaceful Places is an Android app for finding the places where you can relax and be at peace

To do this it uses google maps, where the user can add points

If you are interested in this project please contact me: tord (dot) dellsen (at) gmail (dot) com
